<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');  
});
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::post('/appointments/store', 'AppointmentController@store');
Route::post('/appointments/update/{id}', 'AppointmentController@update');
Route::get('/appointments/show/{id}', 'AppointmentController@show');
Route::get('/appointments/edit/{id}', 'AppointmentController@edit');
Route::get('/appointments/destroy/{id}', 'AppointmentController@destroy');
Route::get('/appointments/patient', 'AppointmentController@patient');
Route::get('/appointments/doctor', 'AppointmentController@doctor');
Route::resource('appointments', 'AppointmentController');
//users
Route::post('/users/store', 'UserController@store');
Route::post('/users/update/{id}', 'UserController@update');
Route::get('/users/show/{id}', 'UserController@show');
Route::get('/users/edit/{id}', 'UserController@edit');
Route::get('/users/destroy/{id}', 'UserController@destroy');
Route::resource('users', 'UserController');

//medicines

Route::post('/medicines/store', 'MedicineController@store');
Route::post('/medicines/update/{id}', 'MedicineController@update');
Route::get('/medicines/show/{id}', 'MedicineController@show');
Route::get('/medicines/edit/{id}', 'MedicineController@edit');
Route::get('/medicines/destroy/{id}', 'MedicineController@destroy');
Route::resource('medicines', 'MedicineController');

//diagnostics

Route::post('/diagnostics/store', 'DiagnosticController@store');
Route::post('/diagnostics/update/{id}', 'DiagnosticController@update');
Route::get('/diagnostics/show/{id}', 'DiagnosticController@show');
Route::get('/diagnostics/edit/{id}', 'DiagnosticController@edit');
Route::get('/diagnostics/destroy/{id}', 'DiagnosticController@destroy');
Route::get('/diagnostics/patient', 'DiagnosticController@patient');
Route::resource('diagnostics', 'DiagnosticController');


//Prescritions

Route::post('/prescriptions/store', 'PrescriptionController@store');
Route::post('/prescriptions/update/{id}', 'PrescriptionController@update');
Route::get('/prescriptions/show/{id}', 'PrescriptionController@show');
Route::get('/prescriptions/edit/{id}', 'PrescriptionController@edit');
Route::get('/prescriptions/destroy/{id}', 'PrescriptionController@destroy');
Route::get('/prescriptions/patient', 'PrescriptionController@patient');
Route::resource('prescriptions', 'PrescriptionController');

//MedicinesPrescription

Route::get('/medipres/create/{id}', 'MedicinePrescritionController@create');
Route::post('/medipres/create/store', 'MedicinePrescritionController@store');
Route::post('/medipres/update/{id}', 'MedicinePrescritionController@update');
Route::get('/medipres/show/{id}', 'MedicinePrescritionController@show');
Route::get('/medipres/edit/{id}', 'MedicinePrescritionController@edit');
Route::get('/medipres/destroy/{id}', 'MedicinePrescritionController@destroy');
Route::resource('medipres', 'MedicinePrescritionController');
