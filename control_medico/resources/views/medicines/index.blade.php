@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerMedi')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Lista de medicamentos</h4>
	</div>

	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Cantidad</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				@foreach($medicines as $medicine)
				<tr>
					<td>{{ $medicine->id }}</td>
					<td>{{ $medicine->name }}</td>
					<td>{{ $medicine->remark }}</td>
					<td>{{ $medicine->quantity }}</td>
					<td>
						<a href="/medicines/show/{{ $medicine->id }}"><span class="label label-info">Ver</span></a>
						<a href="/medicines/edit/{{ $medicine->id }}"><span class="label label-success">Editar</span></a>
						<a href="{{ url('/medicines/destroy',$medicine->id) }}"><span class="label label-danger">Eliminar</span></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif
