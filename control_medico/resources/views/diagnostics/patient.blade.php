@include('security.headerDiag')
<div class="panel panel-success">
	<div class="panel-heading">
		<h4>Lista de citas</h4>
	</div>

	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Id</th>
					<th>Fecha</th>
					<th>Descripción</th>
					<th>Médico</th>
					<th>Paciente</th>
				</tr>
			</thead>
			<tbody>
				@foreach($diagnostic as $diagnostic)
				<tr>
					@if(Auth::user()->id == $diagnostic->id_patient)
					<td>{{ $diagnostic->id }}</td>
					<td>{{ $diagnostic->date }}</td>
					<td>{{ $diagnostic->remark }}</td>
					<td>{{ $diagnostic->id_doctor }}</td>
					<td>{{ $diagnostic->id_patient }}</td>
					@endif
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>