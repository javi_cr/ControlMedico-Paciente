
<!doctype html>
<html ang="{{ app()->getLocale() }}">
<head>
	<title>Citas</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<style>
		body {
			width: 950px;
			margin: 150px auto;
		}
		.badge {
			float: right;
		}
	</style>
</head>
<body>
	<h1>Citas</h1>
	<nav class="navbar navbar-default" role="navigation">
  		<div class="container-fluid">
  			<div class="navbar-header">
				<a class="navbar-brand" href="#">R&H projects</a>
  			</div>
    		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      			<ul class="nav navbar-nav">
      				@if( Auth::user()->typeUser == "secretaria")
      				<li><a href="/appointments">Todas</a></li>
              <li><a href="/appointments/create">Nueva</a></li>
        			<li><a href="/home">Home</a></li>
      				@elseif( Auth::user()->typeUser == "patient")
      				<li><a href="/appointments/patient">Todas</a></li>
        			<li><a href="/home">Home</a></li>
              @else
              <li><a href="/appointments/doctor">Todas</a></li>
              <li><a href="/home">Home</a></li>
      				@endif
        			
        		</ul>
        	</div>
        </div>
    </nav>