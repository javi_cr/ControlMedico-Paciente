@include('security.headerApooi')

<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Información de la cita</h4>
 </div>

 <div class="panel-body">
  @if (!empty($appointment))
  <p>
    Creada: <strong>{{ $appointment->created_appoi}}</strong>
  </p>
  <p>
    Fecha de cita: <strong>{{ $appointment->date_apoint }}</strong>
  </p>
  <p>
    Doctor: <strong>{{ $appointment->id_doctor}}</strong>
  </p>
  <p>
    Paciente: <strong>{{ $appointment->id_patient}}</strong>
  </p>
  @else
  <p>
    No existe información para esta cita.
  </p>
  @endif
  @if(Auth::user()->id == $appointment->id_doctor)
  <a href="/appointments/doctor" class="btn btn-default">Regresar</a>
  @elseif(Auth::user()->id == $appointment->id_patient)
  <a href="/appointments/patient" class="btn btn-default">Regresar</a>
  @else
  <a href="/appointments" class="btn btn-default">Regresar</a>
  @endif
</div>
</div>
</body>
</html>