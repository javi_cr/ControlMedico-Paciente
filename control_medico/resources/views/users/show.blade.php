@if(Auth::user()->typeUser != "administrador")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerUser')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Información de la cita</h4>
 </div>

 <div class="panel-body">
  @if (!empty($user))
  <p>
    Nombre: <strong>{{ $user->name}}</strong>
  </p>
  <p>
    Apellido: <strong>{{ $user->lastName }}</strong>
  </p>
  <p>
    Fecha de Nacimiento: <strong>{{ $user->dateBorn}}</strong>
  </p>
  <p>
    Tipo de Usuario: <strong>{{ $user->typeUser}}</strong>
  </p>
  <p>
    Sexo: <strong>{{ $user->sexo}}</strong>
  </p>
  <p>
    Correo electrónico: <strong>{{ $user->email}}</strong>
  </p>
  @else
  <p>
    No existe información para esta cita.
  </p>
  @endif
  <a href="/appointments" class="btn btn-default">Regresar</a>
</div>
</div>
</body>
</html>
@endif