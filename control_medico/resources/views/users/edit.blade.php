@if(Auth::user()->typeUser != "administrador")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerUser')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Editar Usuario</h4>
 </div>

 <div class="panel-body">
  @if (!empty($user))
  <form method="post" action="/users/update/{{ $user->id }}">
    <p>
      <label>Nombre</label> <br>
      <input value="{{ $user->name }}" type="text" name="name" required autofocus>
    </p>
    <p>
      <label>Apellido</label> <br>
      <input value="{{ $user->lastName }}" type="text" name="lastName" required autofocus>
    </p>
    <p>
      <label>Sexo</label> <br>
      @if($user->sexo == 'F')
      <input id="sexo" type="radio" name="sexo" value="F" checked>F
      <input id="sexo" type="radio" name="sexo" value="M" >M
      @else
      <input id="sexo" type="radio" name="sexo" value="F" >F
      <input id="sexo" type="radio" name="sexo" value="M" checked>M
      @endif
    </p>
    <p>
      <label>Fecha de nacimiento</label> <br>
      <input value="{{ $user->dateBorn }}" type="date" name="dateBorn" required autofocus>
    </p>
    <p>
      <label>Tipo de usuario</label> <br>
      <SELECT id="typeUser" name="typeUser">
        <option>{{ $user->typeUser }}</option>
        <option value="medico">MEDICO</option>
        <option value="secretaria">SECRETARIA</option>
        <option value="paciente">PACIENTE</option>
      </SELECT>

    </p>
    <p>
      <label>Correo electrónico</label><br>
      <input value="{{ $user->email }}" type="email" name="email" required>
    </p>
    <p>
      <label>Contraseña</label><br>
      <input value="{{ $user->password }}" id="password" type="password" name="password" required>
    </p>
    <p>
      <label>Confirmar Contraseña</label><br>
      <input value="{{ $user->password }}" id="password" type="password" name="confirmar" required>
    </p>
    <input type="submit" value="Guardar" class="btn btn-success">
    @else
    <p>
      No existe información para éste para la cita.
    </p>
    @endif
    <a href="/appointments" class="btn btn-default">Regresar</a>
  </form>
</div>
</div>

@if(Session::has('message'))
<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>
@endif