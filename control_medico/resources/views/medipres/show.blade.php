@if(Auth::user()->typeUser != "medico")
<h2>No tienes permiso para entrar en esta página</h2>
<a href="/home">Volver</a>
@else
@include('security.headerDiag')
<div class="panel panel-success">
  <div class="panel-heading">
   <h4>Información del diagnóstico</h4>
 </div>

 <div class="panel-body">
  @if (!empty($diagnostic))
  <p>
    Fecha: <strong>{{ $diagnostic->date}}</strong>
  </p>
  <p>
    Descripción: <strong>{{ $diagnostic->remark }}</strong>
  </p>
  <p>
    Doctor: <strong>{{ $diagnostic->id_doctor}}</strong>
  </p>
  <p>
    Paciente: <strong>{{ $diagnostic->id_patient}}</strong>
  </p>
  @else
  <p>
    No existe información para esta cita.
  </p>
  @endif
  <a href="/diagnostics" class="btn btn-default">Regresar</a>
</div>
</div>
</body>
</html>
@endif