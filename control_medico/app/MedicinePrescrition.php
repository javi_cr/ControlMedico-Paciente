<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MedicinePrescrition extends Model
{
    protected $fillable=['id','id_medicine', 'id_prescription',];
}
