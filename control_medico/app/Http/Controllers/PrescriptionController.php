<?php

namespace App\Http\Controllers;

use App\Prescription;
use App\User;
use App\MedicinePrescrition;
use App\Medicine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;

class PrescriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prescription = Prescription::orderBy('id', 'DESC')->get();
        return view('prescriptions.index')->with('prescription',$prescription);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function patient()
    {
        $prescription = Prescription::orderBy('id', 'DESC')->get();
        return view('prescriptions.patient')->with('prescription',$prescription);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$users = User::orderBy('id', 'DESC')->get();
        return view('prescriptions.create')->with("users", $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $prescription = new Prescription;
        $prescription->name = Input::get('name');
        $prescription->id_patient = Input::get('id_patient');
        $prescription->id_doctor = Input::get('id_doctor');
        if ($prescription->save()) {
            Session::flash('message', 'Guardado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('prescriptions/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $mepre = MedicinePrescrition::orderBy('id', 'DESC')->get();
        $medicine = Medicine::orderBy('id', 'DESC')->get();
        $prescription = Prescription::find($id);
        return view('prescriptions.show')->with('prescription', $prescription)->with('medipres', $mepre)->with('medicines', $medicine)  ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\$Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $prescription = Prescription::find($id);
        $prescription->name = Input::get('name');
        $prescription->id_patient = Input::get('id_patient');
        $prescription->id_doctor = Input::get('id_doctor');

        if ($prescription->save()) {
            Session::flash('message', 'Actualizado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('prescriptions/edit/'.$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function edit($id = null)
    {
        $prescription = Prescription::find($id);
        return view('prescriptions.edit')->with('prescription', $prescription);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medicine  $medicine
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prescription = Prescription::find($id);
        if ($prescription->delete()) {
            Session::flash('message', 'Eliminado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('prescriptions');

    } 
}
