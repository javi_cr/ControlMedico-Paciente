<?php

namespace App\Http\Controllers;

use App\MedicinePrescrition;
use App\Prescription;
use App\Medicine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Illuminate\Support\Facades\Redirect;


class MedicinePrescritionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        $medicine = Medicine::orderBy('id', 'DESC')->get();
        $prescription = Prescription::find($id);
        return view('medipres/create')->with('prescription', $prescription)->with('medicines', $medicine);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $medipre = new MedicinePrescrition;
        $medipre->id_medicine = Input::get('id_medicine');
        $medipre->id_prescription = Input::get('id_prescription');
        if ($medipre->save()) {
            Session::flash('message', 'Guardado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('prescriptions');
    }
     /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $medipre = MedicinePrescrition::find($id);
        return view('medipres.show')->with('medipres', $medipre);
    }

}
