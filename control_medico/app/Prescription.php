<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $fillable=['id','name', 'id_doctor', 'id_patient',];
}
